/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.SpringBoot20220511Test;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author lendle
 */
@RestController
public class NoteController {
    private static List<Note> notes=List.of(
            new Note("id1", "8日起取消全校停課 國高中匡列確診者九宮格同學", "20220507", "教育部長潘文忠今天下午出席中央流行疫情指揮中心記者會，教育部也同步發出新聞稿說明，8日起取消原「全校1/3或10班以上班級有確診者或密切接觸者，得實施全校暫停實體課程」規定，但學校仍可考量運作量能，因應調整學校授課方式。"),
            new Note("id2", "防疫新制今上路 這類人取消電子圍籬 5大QA一次看", "20220508", "本土確診指數型成長，疫情指揮中心宣布調整密切接觸者匡列原則，將原本的同住親友、同班同學、同辦公室或同工作場域接觸者，縮減為僅限同住親友，職場及學校則採自主應變，同時也取消居家隔離者電子圍籬管制，今日起實施。另外確診個案居家照護也將原本的10+7改為7+7，《中時新聞網》整理5大QA一次看。"),
            new Note("id3", "網瘋傳「餐廳歇業下月恐破產」", "20220508", "中國大陸受到疫情影響，有些地區面臨封城，餐廳也無法內用，汪小菲經營新的餐飲品牌「麻六記」，在上海、北京開了多家分店，也因為疫情的關係，餐廳開放外賣，汪小菲也常會上直播帶貨，推銷自家餐廳的料理包，不過日前網路上傳出餐廳出現經營不善的情況，下個月恐破產，讓他忍不住在微博發文澄清「現在麻六記線上的銷售和外賣已經遠遠超過線下店的營收」，而且也強調「我們是一個食品公司並不僅是一個線下連鎖店」。並且豪氣表示等到解封後，還有五家店要開業。")
    );
    @GetMapping("/")
    public ModelAndView getNotesPage(){
        return new ModelAndView("index");
    }
    @GetMapping("/note/{id}")
    public ModelAndView getNotePage(@PathVariable String id){
        Map<String, String> map=Map.of("id", id);
        return new ModelAndView("note", map);
    }
    @GetMapping("/api/notes")
    public List<Note> getNotes(){
        return notes;
    }
    
    @GetMapping("/api/note/id/{id}")
    public Note getNote(@PathVariable String id){
        
    }
}
