package com.example.SpringBoot20220511Test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot20220511TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot20220511TestApplication.class, args);
	}

}
